package be.liantis.serenity.report.out;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class CucumberReportClient {
    private RestTemplate restTemplate;

    public CucumberReportClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public FeatureResource[] getFeatures() {
        return restTemplate.getForObject("http://localhost:9000/test-report.json", FeatureResource[].class);
    }
}
