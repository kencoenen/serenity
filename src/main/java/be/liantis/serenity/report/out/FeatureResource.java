package be.liantis.serenity.report.out;

import be.liantis.serenity.report.Feature;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class FeatureResource {
    private String name;
    private List<ScenarioResource> elements = new ArrayList<>();

    public FeatureResource() {
    }

    public FeatureResource(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ScenarioResource> getElements() {
        return elements;
    }

    public void setElements(List<ScenarioResource> elements) {
        this.elements = elements;
    }

    public Feature toFeature() {
        Feature feature = new Feature(name);
        feature.setScenarios(elements.stream().map(ScenarioResource::toScenario).collect(toList()));
        return feature;
    }
}
