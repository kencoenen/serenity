package be.liantis.serenity.report.out;

import be.liantis.serenity.report.Step;

public class StepResource {
    private String name;
    private ResultResource result;
    private String keyword;

    public StepResource() {
    }

    public StepResource(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ResultResource getResult() {
        return result;
    }

    public void setResult(ResultResource result) {
        this.result = result;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public Step toStep() {
        return new Step(keyword.concat(" ").concat(name), result.getStatus());
    }
}
