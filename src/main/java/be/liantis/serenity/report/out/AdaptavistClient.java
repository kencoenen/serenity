package be.liantis.serenity.report.out;

import be.liantis.serenity.SerenityApplication;
import be.liantis.serenity.adaptavist.TestCycle;
import com.google.common.io.Resources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class AdaptavistClient {
    private static final Logger log = LoggerFactory.getLogger(AdaptavistClient.class);

    private RestTemplate restTemplate;

    public AdaptavistClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public static void main(String[] args) {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);
            MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();

            Path reportFile = Paths.get(Resources.getResource("static/test-report.json").toURI());
            body.add("file", new FileSystemResource(reportFile));

            HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

            ResponseEntity<TestCycle> testCycle = new RestTemplate().postForEntity("http://localhost:9000/automation/execution/cucumber/JQA-C37", requestEntity, TestCycle.class);
            log.info("Test Run with key {} created", testCycle.getBody().getKey());
        } catch (URISyntaxException e) {
            log.error("Something went wrong", e);
        }
    }
}
