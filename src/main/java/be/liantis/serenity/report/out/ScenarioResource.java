package be.liantis.serenity.report.out;

import be.liantis.serenity.report.Scenario;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public class ScenarioResource {
    private String name;
    private List<StepResource> steps = new ArrayList<>();

    public ScenarioResource() {
    }

    public ScenarioResource(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<StepResource> getSteps() {
        return steps;
    }

    public void setSteps(List<StepResource> steps) {
        this.steps = steps;
    }

    public Scenario toScenario() {
        Scenario scenario = new Scenario(name);
        scenario.setSteps(steps.stream().map(StepResource::toStep).collect(toList()));
        return scenario;
    }
}
