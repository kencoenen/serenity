package be.liantis.serenity.report.out;

public class ResultResource {
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
