package be.liantis.serenity.report;

import java.util.ArrayList;
import java.util.List;

public class Feature {
    private String name;
    private List<Scenario> scenarios = new ArrayList<>();

    public Feature(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Scenario> getScenarios() {
        return scenarios;
    }

    public void setScenarios(List<Scenario> scenarios) {
        this.scenarios = scenarios;
    }
}
