package be.liantis.serenity.report;

import be.liantis.serenity.report.out.CucumberReportClient;
import be.liantis.serenity.report.out.FeatureResource;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;

@Service
public class ReportService {
    private CucumberReportClient cucumberReportClient;

    public ReportService(CucumberReportClient cucumberReportClient) {
        this.cucumberReportClient = cucumberReportClient;
    }

    public Report getReport() {
        FeatureResource[] featuresArray = cucumberReportClient.getFeatures();
        Report report = new Report();
        List<FeatureResource> featureResources = asList(featuresArray);
        List<Feature> features = featureResources.stream()
                .map(FeatureResource::toFeature)
                .collect(Collectors.toList());
        report.setFeatures(features);
        return report;
    }
}
