package be.liantis.serenity;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class SerenityApplication {
    private static final Logger log = LoggerFactory.getLogger(SerenityApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(SerenityApplication.class, args);
		log.debug("Application started");
	}

	@Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

}

