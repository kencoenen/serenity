package be.liantis.serenity.adaptavist;

import be.liantis.serenity.SerenityApplication;
import be.liantis.serenity.report.out.FeatureResource;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.micrometer.core.instrument.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Iterator;

import static org.springframework.http.ResponseEntity.created;

@RestController
public class AdaptavistController {
    private static final String JIRA_URL = "http://your-jira-host:port/your-jira-context/secure/Tests.jspa";
    private static final Logger log = LoggerFactory.getLogger(SerenityApplication.class);

    private ObjectMapper objectMapper;

    public AdaptavistController(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @PostMapping("/automation/execution/cucumber/{projectKey}")
    public ResponseEntity<TestCycle> createTestRun(MultipartHttpServletRequest request, @PathVariable("projectKey") String projectKey) throws IOException {
        Iterator<String> itr = request.getFileNames();
        MultipartFile mpf;

        while (itr.hasNext()) {
            mpf = request.getFile(itr.next());
            log.info("Received file {}", mpf.getName());
            String reportContent= IOUtils.toString(mpf.getInputStream(), StandardCharsets.UTF_8);
            log.info(reportContent);
            FeatureResource[] featureResources = objectMapper.readValue(reportContent, FeatureResource[].class);
            Arrays.asList(featureResources).forEach(featureResource -> {
                log.info("Identified feature {}", featureResource.getName());
            });
        }
        URI location = ServletUriComponentsBuilder.fromHttpUrl(JIRA_URL).build(new Object[]{});
        return created(location).body(new TestCycle("JQA-C37", JIRA_URL));
    }

}
