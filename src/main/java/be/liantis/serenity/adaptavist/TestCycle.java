package be.liantis.serenity.adaptavist;

import java.util.Random;

public class TestCycle {
    private Long id = new Random().nextLong();
    private String key;
    private String url;

    public TestCycle() {
    }

    public TestCycle(String key, String url) {
        this.key = key;
        this.url = url;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
