DROP TABLE IF EXISTS users;

CREATE TABLE users(
  id UUID NOT NULL,
  name VARCHAR(128) NOT NULL
);